<?php 
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
    header("Access-Control-Allow-Headers: X-Requested-With");
    header('Content-Type: text/html; charset=utf-8');
    header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

    require '../config.php';

    \Slim\Slim::registerAutoloader();
    $app = new \Slim\Slim( array(
        'mode' => 'development' // development Or production
    ));

    $app->group('/api', function() use ($app){
        // List Clients in database
        $app->get('/listClients', 'authenticate', function() use ($app){
            $mysqli = new dbConect();
            $reponse = array();

            $clients = $mysqli->getSelect("SELECT * FROM client ORDER BY id DESC");

            if(count($clients) == 0){
                $response['found'] = 0;
                $response['message'] = 'no existe registro de usuarios';
            }else{
                foreach($clients as $client){
                    $response['clients'][$client['id']]['uniid'] = $client['uniid'];
                    $response['clients'][$client['id']]['status'] = $client['status'];
                    $response['clients'][$client['id']]['type'] = $client['type'];
                    $response['clients'][$client['id']]['url'] = $client['url'];
                    $response['clients'][$client['id']]['ip'] = $client['ip'];
                }
            }
            echoResponse($response);
        });
        
        $app->get('/client/:uniid(/:data)', 'authenticate', function($uniid, $meta = null) use ($app){
            global $mysqli;
            $mysqli = new dbConect();
            $response = array();
            
            $client = $mysqli->getSelect("SELECT * FROM client WHERE uniid = '{$uniid}'");

            if(count($client) == 0){
                $response['found'] = 0;
                $response['message'] = 'No existes';
            }else{
                $response['uniid'] = $client[0]['uniid'];
                $response['status'] = $client[0]['status'];
                $response['date'] = $client[0]['type'];
                $response['url'] = $client[0]['url'];
                $response['ip'] = $client[0]['ip'];
            }

            if($meta == "data"){
                $response['data'] = getDatas($uniid);
            }

            echoResponse($response);
        });

        // Obtain ID
        $app->get('/obtainId', function() use ($app){
            session_start();
            $response = array();

            if(isset($_SESSION['client_id'])){
                $response['found'] = 1;
                $response['client_id'] = $_SESSION['client_id'];
            }else{
                $response['found'] = 0;
                $response['message'] = 'No ha iniciado session';
            }

            echoResponse($response);
        });

        $app->post('/generateuniid', function() use ($app){
            $submit = $app->request->post('submit');

            if($submit == null){
                $response['found'] = 0;
                $response['message'] = 'No existes';
            }else{
                $response['found'] = 1;
                $response['clientid'] = generateunid();
            }
            echoResponse($response);
        });
        
        $app->post('/online', 'authenticate', function() use ($app){
            $uniid = $app->request->post('uniid');

            $mysqli = new dbConect();

            $status = 1;

            $mysqli->query("UPDATE client SET status = {$status} WHERE uniid = '{$uniid}'");
        });

        $app->get('/test', function() use ($app){
            
            $test = $app->request->get('test');
            $uniid = $app->request->get('uniid');

            if($test == "yes"){
                $mysqli = new dbConect();

                $result = $mysqli->getData($uniid,'Code-M1');

                echoResponse($result);
            }
        });
    });

    $app->run();
?>