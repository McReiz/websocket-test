<?php
    function getRealIpAddr(){
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
    
        return $ipaddress;
    }
    function generateunid(){
        $bytes = bin2hex(random_bytes(8));
        //$bytes = "343ree345ds";

        $mysqlix = new dbConect();
        
        //$client = '';
        $client = $mysqlix->getSelect("SELECT * FROM client WHERE uniid = '{$bytes}'");

        if(count($client)>0){
            generateunid();
        }else{
            return $bytes;
        }
    }
    function authenticate(\Slim\Route $route) {
        // Getting request headers
        $headers = apache_request_headers();
        $response = array();
        $app = \Slim\Slim::getInstance();
         
        // Verifying Authorization Header
        if (isset($headers['Authorization'])) {
            //$db = new DbHandler(); //utilizar para manejar autenticacion contra base de datos
            
            // get the api key
            $token = $headers['Authorization'];
            
            // validating api key
            if (!($token == API_KEY)) { //API_KEY declarada en Config.php
            
                // api key is not present in users table
                $response["error"] = true;
                $response["message"] = "Acceso denegado. Token inválido";
                echoResponse($response,401);
                
                $app->stop(); //Detenemos la ejecución del programa al no validar
            
            }
        } else {
            // api key is missing in header
            $response["error"] = true;
            $response["message"] = "Falta token de autorización";
            echoResponse($response, 400);
            
            $app->stop();
        }
    }

    function echoResponse($response, $status_code = 200) {
        $app = \Slim\Slim::getInstance();
        // Http response code
        $app->status($status_code);
         
        // setting response content type to json
        $app->contentType('application/json');
         
        echo json_encode($response);
    }

    function getDatas($uniid){
        $mysqli = new dbConect();
        $array = array();

        $array['data_01'] = $mysqli->getData($uniid,'Number-user');
        $array['data_02'] = $mysqli->getData($uniid,'Number-house');
        $array['data_03'] = $mysqli->getData($uniid,'Code-M1');
        $array['data_04'] = $mysqli->getData($uniid,'Code-M2');
        $array['data_05'] = $mysqli->getData($uniid,'Code-Pin');

        return $array;
    }

    function disconnect($uniid){
        $mysqli = new dbConect();

        $query = $mysqli->query("UPDATE client SET status = 0 WHERE uniid = '{$uniid}'");

        if(!$query){
            echo "Falló la creación de la tabla: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }

?>