<?php

class dbConect
{
    private $_server;
    private $_user;
    private $_pass;
    private $_db;

    private $_mysqli;

    function __construct($sv = DB_HOST, $us = DB_USER, $pss = DB_PASS, $db = DB_NAME){
        $this->_mysqli = new mysqli($sv,$us,$pss,$db);
    }

    public function query($arg){
        return $this->_mysqli->query($arg);
    }
    public function prepare($arg){
        return $this->_mysqli->prepare($arg);
    }

    public function getSelect($arg){
        $array = array();
        $query = $this->_mysqli->query($arg);

        while($row = $query->fetch_assoc()){
            $array[] = $row;
        }
        $this->_mysqli->close();

        return $array;
    }
    public function countQuery($from, $order = 'id', $where = null){
        $query = $this->_mysqli->query("SELECT * FROM {$from} ORDER BY {$order} DESC");
        $count = $query->num_rows;

        return $count;
    }

    function getData($uniid, $data_name){
        if($row = $this->_mysqli->prepare("SELECT page,data_name,data_values FROM client_datas WHERE uniid = ? AND data_name = ? LIMIT 1")){
            $request = 0;

            $row->bind_param("ss", $uniid,$data_name);
            $row->execute();
            
            $result = $row->get_result();

            if($result === 0){
                $request = "no exist this data";
            }else{
                $request = $result->fetch_assoc();
            }
            $row->close();

            return $request;
        }else{
            return 'error: '.$this->_mysqli->error;
        }
    }

    function __destruct(){
        
    }
}
