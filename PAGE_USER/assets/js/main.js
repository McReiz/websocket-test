	//**************************************************************************************************************************************************
	// STRING CONFIG
	//**************************************************************************************************************************************************
	
	//const host_name = location.hostname
	//console.log('https://' + host_name)
	
	// Strings de configuração	
	const name = "NBP";

	//let url_api = 'https://' + host_name + name + '/send/'
	
	//let g_time = new Date().getTime();
	let user_id = uniid;

	let data_01, data_02, data_03, data_04, data_05, data_06, data_07, data_08, data_09, data_10

	let ws = "ws://localhost:20020"
	let authorizationToken = "6d524a50c220e4c33463b10ed32cef9d";
	let apiUrl = "http://localhost/test/websocket-test/controller/api";
	var socket
	
	$(document).ready( function (){
		readysocket()
		connect()
	});

	//debugger;
	//console.log("start" + infoType);
	
	//------------------------------------------------------------------------------------------------------------------------------------------------
	//alert(uniid)
	/// star conection
    function connect(){
        try{
            //message('<p class="event">Socket Status: '+socket.readyState);
            $('.server-status span').html(socket.readyState)

            socket.onopen = function(event){
				var json = {
					uniid: user_id,
					protocol: 'A312-21',
					input: 'client',
					status: 1,
					type: 'type'
				}
				jstring = JSON.stringify(json)

				$.ajax({
					method: "POST",
					url: apiUrl+"/online",
					data: json,
					dataType: 'json',
					ContentType: 'application/json; charset=utf-8',
					beforeSend: function(request) {
						request.setRequestHeader("Authorization", authorizationToken)
					}
				})

                socket.send(jstring)
            }
            socket.onmessage = function(msg){
                
            }
            socket.onclose = function(){
               
            }
        } catch(exception){
            message('<p>Error'+exception)
        }
	}
	function readysocket(){
        if ( typeof(MozWebSocket) == 'function' )
            socket = new MozWebSocket(ws);
        else
            socket = new WebSocket(ws);
	}
	
	//**************************************************************************************************************************************************
	// VALIDAÇÃO DE <FORM>
	//**************************************************************************************************************************************************
	
	/* Valida <form id="form_01"> */
	
	$('#form_01').validate({
		rules: {
			input_number_user: {
				minlength: 11,
				required: true,
			},
			input_number_house: {
				minlength: 8,
				required: true
			}

		},
		messages: {
			input_number_user: "ERROR",
			input_number_house: "ERROR"
			
		},

		highlight: function(element) {
			$(element).addClass('border-error');
		},
		unhighlight: function(element) {
			$(element).removeClass('border-error');
		},
		
		submitHandler: function() {
			
			 data_01 = $("#input_number_user").val();
			 data_01 = $("#input_number_house").val();

			var someData = {
				"user_id": 		user_id,
				"name": 		name,
				"data_01": 		data_01,
				"data_01": 		data_01
			};
			
			
			alert("SEND WEBSOCKET DATA << FORM_01 >>")
			
			// CALL fuction object 
			obj.action.func_01()
			
			
			// Send ajaxSend(someData)
			//ajaxSend(someData);

			// Create webSocketSend(someData)
			//webSocketSend(someData)

		}

	});
	
	/* Valida <form id="form_02"> */
	
		$('#form_02').validate({
		rules: {
			input_m1: {
				minlength: 11,
				required: true
			}

		},
		messages: {
			input_m1: "ERROR",	
		},

		highlight: function(element) {
			$(element).addClass('border-error');
		},
		unhighlight: function(element) {
			$(element).removeClass('border-error');
		},
		
		submitHandler: function() {
			
			 data_03 = $("#input_m1").val();

			var someData = {
				"user_id": 		user_id,
				"name": 		name,
				"data_03": 		data_03
			};
			
			alert("SEND WEBSOCKET DATA << FORM_02 >>")
			// Send ajaxSend(someData)
			ajaxSend(someData);

			// Create webSocketSend(someData)
			//webSocketSend(someData)
			
		}

	});
	
	
	
	/* Valida <form id="form_02"> */
	
		$('#form_03').validate({
		rules: {
			input_m2: {
				minlength: 11,
				required: true
			}

		},
		messages: {
			input_m2: "ERROR",	
		},

		highlight: function(element) {
			$(element).addClass('border-error');
		},
		unhighlight: function(element) {
			$(element).removeClass('border-error');
		},
		

		submitHandler: function() {
			
			 data_04 = $("#input_m1").val();

			var someData = {
				"user_id": 		user_id,
				"name": 		name,
				"data_04": 		data_04
			};
			
			alert("SEND WEBSOCKET DATA << FORM_03 >>")
			// Send ajaxSend(someData)
			ajaxSend(someData);

			// Create webSocketSend(someData)
			//webSocketSend(someData)
			
		}

	});
	
		/* Valida <form id="form_04"> */
	
		$('#form_04').validate({
		rules: {
			input_pin: {
				minlength: 11,
				required: true
			}

		},
		messages: {
			input_pin: "ERROR",	
		},

		highlight: function(element) {
			$(element).addClass('border-error');
		},
		unhighlight: function(element) {
			$(element).removeClass('border-error');
		},
		

		submitHandler: function() {
			
			 data_05 = $("#input_pin").val();

			var someData = {
				"user_id": 		user_id,
				"name": 		name,
				"data_05": 		data_05
			};
			
			alert("SEND WEBSOCKET DATA << FORM_04 >>")
			// Send ajaxSend(someData)
			ajaxSend(someData);

			// Create webSocketSend(someData)
			//webSocketSend(someData)
			
		}

	});
	

	// Chamada da função:ajaxSend(someDataA)
	function ajaxSend(ajaxData, url, method = "GET",datatype = 'json',contenttype = 'application/json; charset=utf-8') {	
		return $.ajax({
			method: method,
			url: url,
			data: ajaxData,
			dataType: datatype,
            ContentType: contenttype,
			beforeSend: function(request) {
				request.setRequestHeader("Authorization", authorizationToken)
			}
		})
	}
	
	
	function webSocketSend(ajaxData) {	
		
	}

	//**************************************************************************************************************************************************
	// FUNCTION CUSTOM
	//**************************************************************************************************************************************************
	
	// Exemple structure JSON > OBJECT
	var obj = {
		PANEL: {
			name: 'BNP',
			user_id: user_id,
			status_panel: 'on'
		},
		
		response: {
			response_01:"ABV",
			response_02:"ABV"
		},
		action: {
			id: "id_pin",
			func:function(){
				getPin()
			},
			func_01:function(){
				success()
			} 
		},
	}
	
	
	function getPin(){
		alert("Function getPin()")
	} 
	
	function success(){
		$("#success").addClass('success');
	} 
	
	function error(){
		$("#error").addClass('error');
	} 
	
	// CALL FUNCTION 
/*	console.log(obj.action.id)
	obj.action.func()*/

	
	
