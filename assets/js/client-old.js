(function($){
    var clientid
    var host = "ws://localhost:20020"
    var socket

    function obtainClientid(id){
        clientid = id
    }

    if ( typeof(MozWebSocket) == 'function' )
    socket = new MozWebSocket(host);
    else
    socket = new WebSocket(host);

    $(document).ready(function() {
        connect()
        SubmitForm()

        $('.the-id').on('submit', function(event){
            event.preventDefault();

            var datas = {
                id_client: $(this).find('input[name="id_client"]').val(),
                submit: 'get',
                messages: true
            }
            getData(datas);
        })
    })

    function connect(){
        try{
            //message('<p class="event">Socket Status: '+socket.readyState);
            $('.server-status span').html(socket.readyState)

            socket.onopen = function(event){
                console.log(event);
                $('.server-status span').html(socket.readyState)
            }
            socket.onmessage = function(msg){
                $.get('controller/session.php').done( function(data){
                    var json = JSON.parse(msg.data)

                    if(data.client_id == json.client_id){
                        setData(json)
                    }else{
                        console.log("AlGO VA MAL!")
                        console.log(data.client_id)
                        console.log(json.client_id)
                    }
                })
            }
            socket.onclose = function(){
                $('.server-status span').html(socket.readyState+' (Closed)')

                $.get('controller/session.php').done(function(data){
                    OnOff(0,clientid)
                })
            }
        } catch(exception){
            message('<p>Error'+exception)
        }
    }

    function SubmitForm(){
        $('#submit-form').submit( function(e){
            e.preventDefault;
            
            var datas = {
                client_id: $("input[name=id_client]").val(),
                date: dateFormat(),
                msg: $("#campo1").val(),
                SEND: 'yes'
            }

            jstring = JSON.stringify(datas)

            $.ajax({
                method: 'POST',
                url: 'controller/comunnique.php',
                dataType: 'json',
                ContentType: 'application/json; charset=utf-8',
                data: datas
            }).done( function (data){
                socket.send(jstring)
            }).fail( function(jqXHR, textStatus, errorThrown){

            })
        })
    }

    function setData(data) {
        var toClone = $('.copy-element .msg')
        console.log(data)
        var setData = toClone.clone().appendTo('.msgs');
 
        setData.find('.client').attr('id',data.client_id)
        setData.find('.client').html(data.client_id)
        setData.find('.date').html(data.date+ " - ")
        setData.find('.message').html(data.msg)
    }

    function getData(datas){
                
        $.ajax({
            method: 'GET',
            url: 'controller/instance.php',
            dataType: 'json',
            ContentType: 'application/json; charset=utf-8',
            data: datas
        }).done(function(data){
            console.log(data)

            var toClone = $('.copy-element .msg');

            if(data.result == true){
                $('#submit-form').removeClass('disable')
                $('.the-id').addClass('disable')

                $.each(data.msg, function(key,value){                    
                    var setData = toClone.clone().appendTo('.msgs');
                    setData.find('.client').attr('id',value.client_id)
                    setData.find('.client').html(value.client_id)
                    setData.find('.date').html(value.date+ " - ")
                    setData.find('.message').html(value.msg)
                })

                $('.msgs').removeClass('disable')
                
            }else{
                $('.result').html('no existe tu id')
            }

            $.get('controller/session.php').done( function(data){
                obtainClientid(data.client_id)
                OnOff(1,clientid)
            })
        }).fail( function(jqXHR, textStatus, errorThrown ){
            console.log('none')
            console.log(datas)

            console.log("////")

            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        })
    }

    function OnOff(stts, id){

        var datas = ({
            client_id: id,
            status: stts,
            submit: 'yes'
        })

        $.ajax({
            method: 'POST',
            url: 'controller/online.php',
            dataType: 'json',
            ContentType: 'application/json; charset=utf-8',
            data: datas
        }).done( function(date){
            
        }).fail( function(jqXHR, textStatus, errorThrown ){
            console.log('none')
            console.log(datas)

            console.log("////")

            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        })

    }

    function message(msg){
        $('#chatLog').append(msg+'</p>');
    }

    function dateFormat(){
        var d = new Date()
        var date

        date = d.getFullYear()
        date+= "-"
        date+= ((d.getMonth() + 1) < 10 ? '0' : '') + (d.getMonth() + 1) 
        date+= "-" 
        date+= (d.getDate() < 10 ? '0' : '') + d.getDate()
        date+=" "
        date+= (d.getHours() < 10 ? '0' : '') + d.getHours()
        date+= ":"
        date+= (d.getMinutes() < 10 ? '0' : '') + d.getMinutes()
        date+=":" 
        date+= (d.getSeconds() < 10 ? '0' : '') + d.getSeconds()

        return date 
    }
})(jQuery)