(function($){
    var clientid
    let apiUrl = "http://localhost/test/websocket-test/controller/api";
    let authorizationToken = "6d524a50c220e4c33463b10ed32cef9d";
    let ws = "ws://localhost:20020"
    var socket

    function obtainClientid(id){
        clientid = id
    }

    function readysocket(){
        if ( typeof(MozWebSocket) == 'function' )
            socket = new MozWebSocket(ws);
        else
            socket = new WebSocket(ws);
    }
    function connect(){
        try{
            //message('<p class="event">Socket Status: '+socket.readyState);
            $('.server-status span').html(socket.readyState)

            socket.onopen = function(event){
                //console.log(event);
            }
            socket.onmessage = function(msg){
                var json = JSON.parse(msg.data)

                var tr = $('#'+json.uniid)
                var datastatus = tr.attr('data-status')
                var status = (json.status == 1) ? 'fas fa-circle text-c-green f-10' : 'fas fa-circle f-10'
                var buttonLabel = (json.status == 1) ? 'Operate' : 'View'
                var elem = $('#table-users.user-online .task-page')
                var elem2 = $('#table-users.user-offline .task-page')

                var elementSelect = (json.status == 1) ? elem : elem2

                elem.find('.odd').find('.dataTables_empty').parent().remove()
                elem2.find('.odd').find('.dataTables_empty').parent().remove()

                if(datastatus != json.status){
                    tr.toggleClass('tr-link-alert tr-link')
                    tr.appendTo( (elementSelect) )
                    tr.find('i').attr('class',status)
                    tr.find('button').html(buttonLabel)
                    tr.attr('data-status', json.status)

                    console.log(json);

                    if(json.status == 1)
                        Action.notify()
                }
            }
            socket.onclose = function(event){
               console.log(event);
            }
        } catch(exception){
            message('<p>Error'+exception)
        }
    }
    function petitionFail(jqXHR, textStatus, errorThrown){

        console.log("//// Problems")

        console.log(jqXHR)
        console.log(textStatus)
        console.log(errorThrown)
    }
    
    $(document).ready( function(){
        readysocket()
        connect()
        loadTables()
    })

    function loadTables(){
        var table = $('#table-users')

        if(table.length){
            $.ajax({
                method: 'GET',
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", authorizationToken)
                },
                url: apiUrl + '/listClients',
                dataType: 'json',
                ContentType: 'application/json; charset=utf-8',
                data: {submit: 'send'}
            }).done(function(data){
                console.log(data)

                var elem = $('#table-users.user-online .task-page')
                var elem2 = $('#table-users.user-offline .task-page')

                elem.find('.odd').find('.dataTables_empty').parent().remove()
                elem2.find('.odd').find('.dataTables_empty').parent().remove()

                var count = Object.keys(data.clients).length;
                
                if(count > 0){
                    $.each(data.clients,function(index,value){
                        console.log(value)
                        var to = (value.status == 1) ? elem : elem2
                        var actv = (value.status == 1) ? 'tr-link-alert' : 'tr-link'
                        var buttonLabel = (value.status == 1) ? 'Operate' : 'view'
                        var status = (value.status == 1) ? 'fas fa-circle text-c-green f-10' : 'fas fa-circle f-10'

                        var clone = $('.copy-to-table .copy-this tr').clone().appendTo(to);

                        clone.attr('id',value.uniid)
                        clone.attr('data-status',value.status)
                        clone.toggleClass(actv)
                        clone.find('i').attr('class',status)
                        clone.find('.tr-uunid').html(value.uniid)
                        clone.find('.tr-type').html(value.type)
                        clone.find('.tr-url').html(value.url)
                        clone.find('button').html(buttonLabel)
                        clone.find('button').removeAttr("onclick", null)
                        clone.find('button').attr("onclick", "ChangeLocation("+value.uniid+")")
                        // "location.href='data_user."+uniid+".html'"
                    })
                }
            }).fail(function(qXHR, textStatus, errorThrown){
                petitionFail(qXHR, textStatus, errorThrown)
            })
        }
    }
    var Action = {
        notify: function(){
            $('#notificacao').trigger('play');
        }
    }
})(jQuery)

function ChangeLocation(theid){
    location.href = "data_user."+theid.id+".html"
}