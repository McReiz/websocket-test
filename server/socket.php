<?php
// prevent the server from timing out
set_time_limit(0);

// include the web sockets server script (the server is started at the far bottom of this file)
require '../config.php';
$clients = array();


// when a client sends data to the server
function wsOnMessage($clientID, $message, $messageLength, $binary) 
{
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	// check if message length is 0
	if ($messageLength == 0) {
		$Server->wsClose($clientID);
		return;
	}

	//print_r($clientID);
	echo $message."\n";
	$data = json_decode($message);

	if(isset($data->input) == 'client'){
		$Server->wsClients[$clientID]['input'] = $data->input;
		$Server->wsClients[$clientID]['uniid'] = $data->uniid;
		$Server->wsClients[$clientID]['status'] = $data->status;
	}
	if(isset($data->connection_id)){
		$Server->wsClients[$clientID]['connection_id'] == $data->connection_id;
	}
	if(isset($data->sendto)){
		$Server->wsClients[$clientID]['sendto'] == $data->sendto;
	}

	//The speaker is the only person in the room. Don't let them feel lonely.
	/*if ( sizeof($Server->wsClients) == 1 )
		$Server->wsSend($clientID, "");
	else*/
	//Send the message to everyone but the person who said it

	if(!isset($Server->wsClients[$clientID]['sendto'])){
		foreach ( $Server->wsClients as $id => $client ){
			if( isset($client['input']) == 'admin4232-43' ){
				$Server->wsSend($id,$message);
			}
		}
	}else{
		$Server->wsSend($Server->wsClients[$clientID]['sendto'],$message);
	}
}

// when a client connects
function wsOnOpen($clientID)
{
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	//Send a join notice to everyone but the person who joined
	foreach ( $Server->wsClients as $id => $client ){
		if ( $id != $clientID ){
			//$Server->wsSend($id, "connecting...");
		}
	}
}

// when a client closes or lost connection
function wsOnClose($clientID, $status) {
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6] );

	$jsonstring = '';
	
	if(isset($Server->wsClients[$clientID]['input']) == 'client'){
		$uniid = $Server->wsClients[$clientID]['uniid'];
		$input = $Server->wsClients[$clientID]['input'];

		$jsonstring = '{connection_id: '.$clientID.',uniid":"'.$uniid.'","input":"'.$input.'","status":0,"type":"type"}';

		echo $jsonstring."\n";
		disconnect($uniid);
	}

	/*foreach ( $Server->wsClients as $id => $client ){
		$Server->wsSend($id, $jsonstring);
	}*/
}

$Server = new PHPWebSocket();
$Server->bind('message', 'wsOnMessage');
$Server->bind('open', 'wsOnOpen');
$Server->bind('close', 'wsOnClose');

$Server->wsStartServer('localhost',20020);

?>