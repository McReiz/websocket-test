<?php
    session_start();

    $json = array();

    if(isset($_SESSION['client_id'])){
        $json['connection'] = true;
        $json['client_id'] = $_SESSION['client_id'];
    }else{
        $json['connection'] = false;
    }

    header('Content-Type: application/json');
    echo json_encode($json);