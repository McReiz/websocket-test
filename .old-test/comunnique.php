<?php 

    session_start();

    require '../config.php';

    $json = array();
    // send mensaje
    if(isset($_POST['SEND']) == "yes"){
        if($_POST['client_id'] != $_SESSION['client_id']){
            $json['request'] = false;
            $json['request-message'] = "el ID no corresponde";
        }else{
            $client_id = $_POST['client_id'];
            $date = $_POST['date'];
            $msg = $_POST['msg'];

            $query = $mysqli->query("SELECT * FROM client WHERE client_id = '{$client_id}'");
            $client = $query->fetch_assoc();

            if(count($client) == 0){
                $json['request'] = false;
                $json['request-message'] = "Cliente no existe";
            }else{
                $query = $mysqli->query("INSERT INTO instances(datetime, message, client_id) VALUES ('{$date}', '{$msg}', '{$client_id}')");

                if(!$query){
                    $json['request'] = false;
                    $json['request-message'] = "Problemas en el paraiso. <br>\n ". $mysqli->error;
                }else{
                    $json['request'] = true;
                    $json['request-message'] = "Todo bien, todo correcto";
                }
            }
        }
    }else{
        $json['request'] = false;
        $json['request-message'] = "No existe un send";
    }

    header('Content-Type: application/json');
    echo json_encode($json)
?>