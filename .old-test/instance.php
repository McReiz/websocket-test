<?php

    session_start();
    require '../config.php';

    $json = array();
    // Call Data
    if(isset($_GET) && isset($_GET['submit'])){
        $client_id = $_GET["id_client"];
        
        $query = $mysqli->query("SELECT * FROM client WHERE client_id = '{$client_id}'");
        $client = $query->fetch_assoc();

        if(count($client) == 0){
            $json['result'] = false;
        }else{
            $json['result'] = true;

            $json['client_id'] = $client_id;
            $json['status'] = $client['status'];
            
            $_SESSION['client_id'] = $_GET["id_client"];

            if((isset($_GET['messages'])) == true){
                $json['msg'] = array();

                $result = $mysqli->query("SELECT * FROM instances WHERE client_id = '{$client_id}' ORDER BY id ASC");
                while($fila = $result->fetch_assoc()){
                    $json['msg'][$fila['id']]['client_id'] = $client_id;
                    $json['msg'][$fila['id']]['date'] = $fila['datetime'];
                    $json['msg'][$fila['id']]['msg'] = $fila['message'];
                }
            }
            $query = $mysqli->query("UPDATE client SET status = 1 WHERE client_id = '{$client_id}'");
        }

        $mysqli->close();
    }
    header('Content-Type: application/json');
    echo json_encode($json);
?>